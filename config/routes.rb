TwitterDemo::Application.routes.draw do
  
  get "tweets/index"
  get "tweet", to: "tweets#new"

  root "tweets#index"
end
